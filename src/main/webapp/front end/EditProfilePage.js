
function loadDoc() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var obj = JSON.parse(this.responseText);
            $('input[name="name"]').attr("value", obj.Firstname);
            $('input[name="lastname"]').attr("value", obj.Lastname);
            $('input[name="email"]').attr("value", obj.Email);
            $('input[name="student-ID"]').attr("value", obj.STID);
            $('input[name="ID"]').attr("value", obj.NationalCode);
        }
    };
    xhttp.open("GET", "/Phase1/api/editprofile.json", true);
    xhttp.send();
}