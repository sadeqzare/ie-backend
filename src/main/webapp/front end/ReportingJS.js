$(document).ready(function(){
    $("#myInput").on("keyup", function() {
        let value = $(this).val().toLowerCase();
        $("#ul-id li").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});

function subject() {
    document.getElementById("case-list").style.display = 'block';
    document.getElementById("case-time").style.display = 'none';
    document.getElementById("case-satisfaction").style.display = 'none';
    document.getElementById("case-status").style.display = 'none';
}

function time() {
    document.getElementById("case-list").style.display = 'none';
    document.getElementById("case-time").style.display = 'block';
    document.getElementById("case-satisfaction").style.display = 'none';
    document.getElementById("case-status").style.display = 'none';
}

function satisfy() {
    document.getElementById("case-list").style.display = 'none';
    document.getElementById("case-time").style.display = 'none';
    document.getElementById("case-satisfaction").style.display = 'block';
    document.getElementById("case-status").style.display = 'none';
}

function status() {
    document.getElementById("case-list").style.display = 'none';
    document.getElementById("case-time").style.display = 'none';
    document.getElementById("case-satisfaction").style.display = 'none';
    document.getElementById("case-status").style.display = 'block';
}