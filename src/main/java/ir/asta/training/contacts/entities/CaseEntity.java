package ir.asta.training.contacts.entities;

import ir.asta.training.contacts.manager.CaseStates;
import ir.asta.training.contacts.manager.CaseTypes;

import javax.persistence.*;
@Entity
@Table(name = "Users")
@NamedQueries({
        @NamedQuery(name = "Case.GetMyCase", query = "select c from CaseEntity c where c.User=:User"),
        @NamedQuery(name = "Case.GetAllCases", query = "select c from CaseEntity c"),
        @NamedQuery(name = "Case.GetAgentCases", query = "select c from CaseEntity c where c.Agent=:Agent"),
        @NamedQuery(name = "Case.GetById", query = "select u from CaseEntity u where  u.Id=:Id"),
})
public class CaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    @Column(name = "Title")
    @Basic
    private String Title;
    @Column(name = "Body")
    @Basic
    private String Body;
    @Column(name = "Satisfiction")
    @Basic
    private Integer Satisfiction;

    public String getAgentAction() {
        return AgentAction;
    }

    public void setAgentAction(String agentAction) {
        AgentAction = agentAction;
    }

    @Column(name = "AgentAction")
    @Basic
    private String AgentAction;
    public CaseStates getState() {
        return State;
    }

    public void setState(CaseStates state) {
        State = state;
    }

    @Column(name = "State")
    @Basic
    private CaseStates State;
    @Column(name = "Type")
    @Basic
    private CaseTypes Type;
    @ManyToOne
    private UserEntity User;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getBody() {
        return Body;
    }

    public void setBody(String body) {
        Body = body;
    }

    public Integer getSatisfiction() {
        return Satisfiction;
    }

    public void setSatisfiction(Integer satisfiction) {
        Satisfiction = satisfiction;
    }

    public CaseTypes getType() {
        return Type;
    }

    public void setType(CaseTypes type) {
        Type = type;
    }

    public UserEntity getUser() {
        return User;
    }

    public void setUser(UserEntity user) {
        User = user;
    }

    public UserEntity getAgent() {
        return Agent;
    }

    public void setAgent(UserEntity agent) {
        Agent = agent;
    }

    @ManyToOne
    private UserEntity Agent;
}
