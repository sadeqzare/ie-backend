package ir.asta.training.contacts.entities;

import ir.asta.training.contacts.manager.Roles;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.ws.rs.Consumes;
import java.util.List;

@Entity
@Table(name = "Users")
@NamedQueries(
        {
                @NamedQuery(name = "User.Login", query = "select u from UserEntity u where u.Uname=:username and u.Password=:password"),
                @NamedQuery(name = "User.GetById", query = "select u from UserEntity u where  u.Id=:Id"),
                @NamedQuery(name = "User.GetAgents", query = "select u from UserEntity u where u.Role=:Role"),
                @NamedQuery(name = "User.GetUnRegistered", query = "select u from UserEntity u where u.Role=:Role and u.Registered=:Registered")
        }
        )
public class UserEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long Id;
    @Column(name = "FirstName")
    @Basic
    private String Fname;
    @Basic
    @Column(name = "LastName")
    private String Lname;
    @Basic
    @Column(name = "UserName" , unique = true)
    private String Uname;
    @Basic
    @Column(name = "Email" , unique = true)
    private String Email;
    @Basic
    @Column(name = "NationalID" , unique = true)
    private String NID;
    @Basic
    @Column(name = "Password")
    private String Password;
    @Basic
    @Column(name = "Role")
    private Roles Role;
    @OneToMany(mappedBy = "User")
    private List<CaseEntity> MyCases;

    public List<CaseEntity> getMyCases() {
        return MyCases;
    }

    public void setMyCases(List<CaseEntity> myCases) {
        MyCases = myCases;
    }

    public List<CaseEntity> getRefrencedCases() {
        return RefrencedCases;
    }

    public void setRefrencedCases(List<CaseEntity> refrencedCases) {
        RefrencedCases = refrencedCases;
    }

    @OneToMany(mappedBy = "Agent")
    private List<CaseEntity> RefrencedCases;


    public boolean isRegistered() {
        return Registered;
    }

    public void setRegistered(boolean registered) {
        Registered = registered;
    }

    @Basic
    @Column(name = "Registered")
    private Boolean Registered;
    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getFname() {
        return Fname;
    }

    public void setFname(String fname) {
        Fname = fname;
    }

    public String getLname() {
        return Lname;
    }

    public void setLname(String lname) {
        Lname = lname;
    }

    public String getUname() {
        return Uname;
    }

    public void setUname(String uname) {
        Uname = uname;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getNID() {
        return NID;
    }

    public void setNID(String NID) {
        this.NID = NID;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public Roles getRole() {
        return Role;
    }

    public void setRole(Roles role) {
        Role = role;
    }
}
