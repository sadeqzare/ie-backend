package ir.asta.training.contacts.manager;

import ir.asta.training.contacts.Requests.AddCaseRequest;
import ir.asta.training.contacts.Requests.AgentActionRequest;
import ir.asta.training.contacts.Requests.UserAuthRequest;
import ir.asta.training.contacts.Responses.CaseServiceResponse;
import ir.asta.training.contacts.Responses.GetAgentResponse;
import ir.asta.training.contacts.Responses.GetProfileResponse;
import ir.asta.training.contacts.Responses.UserCaseResponse;
import ir.asta.training.contacts.dao.CaseDao;
import ir.asta.training.contacts.dao.UserDao;
import ir.asta.training.contacts.entities.CaseEntity;
import ir.asta.training.contacts.entities.UserEntity;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;

@Named("caseManager")
public class CaseManager {
    @Inject
    CaseDao caseDao;
    @Inject
    UserDao userDao;
    @Transactional
    public CaseServiceResponse AddCase(AddCaseRequest request){
        CaseEntity entity = new CaseEntity();
        entity.setTitle(request.Title);
        entity.setBody(request.Body);
        entity.setSatisfiction(request.Satisfication);
        entity.setType(request.Type);
        entity.setState(CaseStates.Open);
        entity.setAgentAction("");
        UserEntity agent = userDao.GetById(request.AgentId);
        if(agent == null){
            return new CaseServiceResponse("agent's info is wrong", null);
        }
        if(agent.getRole() != Roles.Employee){
            return new CaseServiceResponse("agent's info is wrong", null);
        }
        UserEntity user = userDao.GetById(request.UserId);
        if(user == null){
            return new CaseServiceResponse("your info is wrong", null);
        }
        String token = HashClass.HashFunction(String.valueOf(user.getId()));

        if(!token.equals(request.UserToken)){
            return new CaseServiceResponse("your identity is not valid" , token);
        }
        entity.setUser(user);
        entity.setAgent(agent);
        if(caseDao.Save(entity)){
            return new CaseServiceResponse("case added successfully", null);
        }else{
            return  new CaseServiceResponse("caseAdd failed", null);
        }
    }
    public ArrayList<GetAgentResponse> GetAgents(){
        ArrayList<UserEntity> result = userDao.GetAgents();
        ArrayList<GetAgentResponse> response = new ArrayList<>();
        for (UserEntity u: result
             ) {
            GetAgentResponse agent = new GetAgentResponse();
            agent.AgentId = u.getId();
            agent.Fname = u.getFname();
            agent.Lname = u.getLname();
            response.add(agent);
        }
        return response;
    }
    public ArrayList<UserCaseResponse> GetUserCase(UserAuthRequest request){
        ArrayList<CaseEntity> result = new ArrayList<>();
        ArrayList<UserCaseResponse> response = new ArrayList<>();
        UserEntity user = userDao.GetById(request.userId);
        if(user == null){
            return response;
        }
        String token = HashClass.HashFunction(String.valueOf(user.getId()));

        if(!token.equals(request.token)){
            return response;
        }

        if(user.getRole() == Roles.Admin){
            result = caseDao.GetAllCases();
        }
        else if(user.getRole() == Roles.Employee){
            result = caseDao.GetAgentCases(user);
        }
        else{
            result = caseDao.GetUserCases(user);
        }
        for (CaseEntity ce: result
             ) {
            UserCaseResponse res = new UserCaseResponse();
            res.Body = ce.getBody();
            res.Id = ce.getId();
            res.State = ce.getState();
            res.Title = ce.getTitle();
            res.Type = ce.getType();
            res.AgentAction = ce.getAgentAction();
            response.add(res);
        }
        return response;
    }
    @Transactional
    public CaseServiceResponse AgentAction(AgentActionRequest request){
        UserEntity user = userDao.GetById(request.UserId);
        if(user == null){
            return new CaseServiceResponse("your info is wrong", null);
        }
        String token = HashClass.HashFunction(String.valueOf(user.getId()));

        if(!token.equals(request.UserToken)){
            return new CaseServiceResponse("your identity is not valid" , token);
        }
        CaseEntity ce = caseDao.GetById(request.CaseId);
        if(ce.getAgent().getId() != user.getId()){
            return new CaseServiceResponse("you have no access on this case.", null);
        }
        ce.setAgentAction(request.Explanation);
        ce.setState(request.State);
        if(caseDao.Save(ce)){
            return new CaseServiceResponse("State updated",null);
        }
        return new CaseServiceResponse("Changing State failed", null);
    }
}
