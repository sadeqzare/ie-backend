package ir.asta.training.contacts.manager;

public enum Roles {
    User,
    Employee,
    Admin
}
