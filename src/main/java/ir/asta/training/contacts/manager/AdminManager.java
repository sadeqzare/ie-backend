package ir.asta.training.contacts.manager;

import ir.asta.training.contacts.Requests.AdminRegisterRequest;
import ir.asta.training.contacts.Responses.AdminServiceResponse;
import ir.asta.training.contacts.Responses.GetUnRegisteredUser;
import ir.asta.training.contacts.Responses.UserServiceResponse;
import ir.asta.training.contacts.dao.UserDao;
import ir.asta.training.contacts.entities.UserEntity;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;

@Named("adminManager")
public class AdminManager {
    @Inject
    UserDao userDao;

    public ArrayList<GetUnRegisteredUser> getUnRegisteredUsers(){
        ArrayList<UserEntity> users = userDao.GetUnRegisteredUsers();
        ArrayList<GetUnRegisteredUser> response = new ArrayList<>();
        for (UserEntity usr: users
             ) {
            GetUnRegisteredUser user = new GetUnRegisteredUser();
            user.Id = usr.getId();
            user.Fname = usr.getFname();
            user.Lname = usr.getLname();
            response.add(user);
        }
        return response;
    }
    @Transactional
    public AdminServiceResponse Register(AdminRegisterRequest request){
        UserEntity user = userDao.GetById(request.Id);
        if(user == null){
            return new AdminServiceResponse("your identity not found", null);
        }

        String token = HashClass.HashFunction(String.valueOf(user.getId()));

        if(!token.equals(request.UserToken)){
            return new AdminServiceResponse("your identity is invalid", token);
        }

        if(user.getRole() != Roles.Admin){
            return new AdminServiceResponse("you are not admin!", null);
        }
        UserEntity agent = userDao.GetById(request.AgentId);
        if(agent == null){
            return new AdminServiceResponse("the user you want to register, not found.", null);
        }

        agent.setRegistered(true);
        userDao.UpdateUser(agent);
        return new AdminServiceResponse("registeration complete.", null);

    }
}
