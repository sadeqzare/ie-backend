package ir.asta.training.contacts.manager;

import ir.asta.training.contacts.Requests.EditProfileRequest;
import ir.asta.training.contacts.Requests.GetProfileRequest;
import ir.asta.training.contacts.Requests.LoginRequest;
import ir.asta.training.contacts.Requests.SignupRequest;
import ir.asta.training.contacts.Responses.*;
import ir.asta.training.contacts.dao.UserDao;
import ir.asta.training.contacts.entities.UserEntity;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.inject.Named;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Named("userManager")
public class UserManager {


    @Inject
    UserDao dao;
    @Transactional
    public AuthResponse Signup(SignupRequest request){
        UserEntity user = new UserEntity();
        user.setFname(request.Fname);
        user.setLname(request.Lname);
        user.setUname(request.Uname);
        user.setEmail(request.Email);
        user.setNID(request.NID);
        user.setPassword(request.Password);
        user.setRole(request.Role);
        if(request.Role == Roles.User){
            user.setRegistered(true);
        }else{
            user.setRegistered(false);
        }
        AuthResponse response = new AuthResponse();
        if(dao.Save(user)){
            response.Message = "Sign up successful";
            response.Token = HashClass.HashFunction(String.valueOf(user.getId()));
            response.role = user.getRole();
            response.Id = user.getId();
        }else{
            response.Message = "Sign up failed";
            response.Token = null;
        }
        return response;
    }

    public AuthResponse Login(LoginRequest request){
        List<UserEntity> users = dao.login(request.Uname,request.Password);
        UserEntity user = users.get(0);
        AuthResponse response = new AuthResponse();
        response.Message = "Login success";
        response.Token = HashClass.HashFunction(String.valueOf(user.getId()));
        response.Id = user.getId();
        return response;
    }
    public GetProfileResponse GetProfile(GetProfileRequest request){
        UserEntity user = dao.GetById(request.UserId);
        GetProfileResponse response = new GetProfileResponse();
        if(user == null){
            response.User = null;
            response.Message = "user not found";
            return response;
        }

        String token = HashClass.HashFunction(String.valueOf(user.getId()));

        if(!token.equals(request.UserToken)){
            response.User = null;
            response.Message = "your identity is not valid";
            return response;
        }

        UserResponse userData = new UserResponse();
        userData.Id = user.getId();
        userData.Fname = user.getFname();
        userData.Lname = user.getLname();
        userData.Uname = user.getUname();
        userData.Email = user.getEmail();
        userData.NationalID = user.getNID();
        userData.Role = user.getRole();

        response.User = userData;
        return response;
    }
    @Transactional
    public UserServiceResponse EditProfile(EditProfileRequest request){
        UserEntity user = dao.GetById(request.UserId);
        if(user == null){
            return new UserServiceResponse("user not found", null);
        }

        String token = HashClass.HashFunction(String.valueOf(user.getId()));

        if(!token.equals(request.UserToken)){
            return new UserServiceResponse("your identity is invalid", token);
        }

        user.setFname(request.Fname);
        user.setLname(request.Lname);
        user.setPassword(request.Password);
        dao.UpdateUser(user);
        return new UserServiceResponse("edit profile done", null);
    }
}
