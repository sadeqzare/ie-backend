package ir.asta.training.contacts.dao;

import ir.asta.training.contacts.entities.CaseEntity;
import ir.asta.training.contacts.entities.UserEntity;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import java.lang.reflect.Array;
import java.util.ArrayList;

@Named("caseDao")
public class CaseDao {
    @PersistenceContext
    private EntityManager entityManager;
    public boolean Save(CaseEntity caseEntity){
        entityManager.persist(caseEntity);
        return true;
    }
    public ArrayList<CaseEntity> GetUserCases(UserEntity user){
        Query query = entityManager.createNamedQuery("Case.GetMyCase");
        query.setParameter("User", user);
        ArrayList<CaseEntity> result = (ArrayList<CaseEntity>) query.getResultList();
        return result;
    }
    public ArrayList<CaseEntity> GetAllCases(){
        Query query = entityManager.createNamedQuery("Case.GetAllCases");
        ArrayList<CaseEntity> result = (ArrayList<CaseEntity>) query.getResultList();
        return result;
    }
    public ArrayList<CaseEntity> GetAgentCases(UserEntity agent){
        Query query = entityManager.createNamedQuery("Case.GetAgentCases");
        query.setParameter("Agent", agent);
        ArrayList<CaseEntity> result = (ArrayList<CaseEntity>) query.getResultList();
        return result;
    }
    public CaseEntity GetById(long Id){
        Query query = entityManager.createNamedQuery("Case.GetById");
        query.setParameter("Id", Id);
        CaseEntity result = (CaseEntity) query.getSingleResult();
        return result;
    }
    public boolean UpdateCase(CaseEntity Case){
        entityManager.merge(Case);
        return true;
    }
}
