package ir.asta.training.contacts.dao;

import ir.asta.training.contacts.Requests.EditProfileRequest;
import ir.asta.training.contacts.entities.UserEntity;
import ir.asta.training.contacts.manager.Roles;

import javax.inject.Named;
import javax.management.relation.Role;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Named("userDao")
public class UserDao {
    @PersistenceContext
    private EntityManager entityManager;
    public boolean Save(UserEntity user){
        entityManager.persist(user);
        return true;
    }

    public List<UserEntity> login(String username, String password){
        Query login = entityManager.createNamedQuery("User.Login");
        login.setParameter("username", username).setParameter("password", password);
        List<UserEntity> resultList = login.getResultList();
        return resultList;
    }
    public UserEntity GetById(Long Id){
        Query query = entityManager.createNamedQuery("User.GetById");
        query.setParameter("Id", Id);
        UserEntity result = (UserEntity) query.getSingleResult();
        return result;
    }
    public boolean UpdateUser(UserEntity user){
        entityManager.merge(user);
        return true;
    }
    public ArrayList<UserEntity> GetAgents(){
        Query query = entityManager.createNamedQuery("User.GetAgents");
        query.setParameter("Role", Roles.Employee);
        ArrayList<UserEntity> result = (ArrayList<UserEntity>) query.getResultList();
        return result;
    }

    public ArrayList<UserEntity> GetUnRegisteredUsers(){
        Query query = entityManager.createNamedQuery("User.GetUnRegistered");
        query.setParameter("Role", Roles.Employee).setParameter("Registered", false);
        ArrayList<UserEntity> result = (ArrayList<UserEntity>) query.getResultList();
        return result;
    }
}
