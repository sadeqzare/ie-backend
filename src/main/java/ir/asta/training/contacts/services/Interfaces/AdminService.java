package ir.asta.training.contacts.services.Interfaces;

import ir.asta.training.contacts.Requests.AdminRegisterRequest;
import ir.asta.training.contacts.Responses.AdminServiceResponse;
import ir.asta.training.contacts.Responses.GetUnRegisteredUser;
import jdk.nashorn.internal.objects.annotations.Getter;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("/admin")
public interface AdminService {
    @GET
    @Path("/getUsers")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<GetUnRegisteredUser> getUnRegisteredUsers();
    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public AdminServiceResponse Register(AdminRegisterRequest request);
}
