package ir.asta.training.contacts.services.Implementations;

import ir.asta.training.contacts.Requests.AdminRegisterRequest;
import ir.asta.training.contacts.Responses.AdminServiceResponse;
import ir.asta.training.contacts.Responses.GetUnRegisteredUser;
import ir.asta.training.contacts.manager.AdminManager;
import ir.asta.training.contacts.manager.UserManager;
import ir.asta.training.contacts.services.Interfaces.AdminService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;

@Named("adminService")
public class AdminServiceImpl implements AdminService {
    @Inject
    AdminManager manager;

    @Override
    public ArrayList<GetUnRegisteredUser> getUnRegisteredUsers() { return manager.getUnRegisteredUsers();}
    public AdminServiceResponse Register(AdminRegisterRequest request){ return manager.Register(request);}
}
