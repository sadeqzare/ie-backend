package ir.asta.training.contacts.services.Implementations;

import ir.asta.training.contacts.Requests.EditProfileRequest;
import ir.asta.training.contacts.Requests.GetProfileRequest;
import ir.asta.training.contacts.Requests.LoginRequest;
import ir.asta.training.contacts.Requests.SignupRequest;
import ir.asta.training.contacts.Responses.AuthResponse;
import ir.asta.training.contacts.Responses.GetProfileResponse;
import ir.asta.training.contacts.Responses.UserResponse;
import ir.asta.training.contacts.Responses.UserServiceResponse;
import ir.asta.training.contacts.manager.UserManager;
import ir.asta.training.contacts.services.Interfaces.UserService;

import javax.inject.Inject;
import javax.inject.Named;

@Named("userService")
public class UserServiceImpl implements UserService {
    @Inject
    UserManager manager;

    @Override
    public AuthResponse Signup(SignupRequest request) {
        return manager.Signup(request);
    }

    @Override
    public AuthResponse Login(LoginRequest request) {
        return manager.Login(request);
    }

    @Override
    public GetProfileResponse GetUser(GetProfileRequest request) { return  manager.GetProfile(request); }

    @Override
    public UserServiceResponse EditProfile(EditProfileRequest request) { return manager.EditProfile(request);}
}
