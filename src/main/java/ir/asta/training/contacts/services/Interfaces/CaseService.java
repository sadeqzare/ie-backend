package ir.asta.training.contacts.services.Interfaces;

import ir.asta.training.contacts.Requests.AddCaseRequest;
import ir.asta.training.contacts.Requests.AgentActionRequest;
import ir.asta.training.contacts.Requests.UserAuthRequest;
import ir.asta.training.contacts.Responses.CaseServiceResponse;
import ir.asta.training.contacts.Responses.GetAgentResponse;
import ir.asta.training.contacts.Responses.UserCaseResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("Cases")
public interface CaseService {
    @POST
    @Path("/addcase")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public CaseServiceResponse AddCase(AddCaseRequest request);
    @GET
    @Path("/getAgents")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<GetAgentResponse> GetAgents();
    @GET
    @Path("/getUserCase")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<UserCaseResponse> GetUserCase(UserAuthRequest request);
    @POST
    @Path("/AgentAction")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public CaseServiceResponse AgentActionOnCase(AgentActionRequest request);
}
