package ir.asta.training.contacts.services.Interfaces;

import ir.asta.training.contacts.Requests.EditProfileRequest;
import ir.asta.training.contacts.Requests.GetProfileRequest;
import ir.asta.training.contacts.Requests.LoginRequest;
import ir.asta.training.contacts.Requests.SignupRequest;
import ir.asta.training.contacts.Responses.AuthResponse;
import ir.asta.training.contacts.Responses.GetProfileResponse;
import ir.asta.training.contacts.Responses.UserResponse;
import ir.asta.training.contacts.Responses.UserServiceResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/auth")
public interface UserService {

    @POST
    @Path("/signup")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public AuthResponse Signup(SignupRequest request);
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public AuthResponse Login(LoginRequest request);
    @GET
    @Path("/profile")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public GetProfileResponse GetUser(GetProfileRequest request);
    @POST
    @Path("/edit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public UserServiceResponse EditProfile(EditProfileRequest request);
}
