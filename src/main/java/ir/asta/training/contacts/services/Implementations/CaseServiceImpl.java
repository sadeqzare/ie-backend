package ir.asta.training.contacts.services.Implementations;

import ir.asta.training.contacts.Requests.AddCaseRequest;
import ir.asta.training.contacts.Requests.AgentActionRequest;
import ir.asta.training.contacts.Requests.UserAuthRequest;
import ir.asta.training.contacts.Responses.CaseServiceResponse;
import ir.asta.training.contacts.Responses.GetAgentResponse;
import ir.asta.training.contacts.Responses.UserCaseResponse;
import ir.asta.training.contacts.manager.CaseManager;
import ir.asta.training.contacts.services.Interfaces.CaseService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;

@Named("caseService")
public class CaseServiceImpl implements CaseService {
    @Inject
    CaseManager manager;
    @Override
    public CaseServiceResponse AddCase(AddCaseRequest request) {
        return manager.AddCase(request);
    }

    @Override
    public ArrayList<GetAgentResponse> GetAgents() { return manager.GetAgents();}

    @Override
    public ArrayList<UserCaseResponse> GetUserCase(UserAuthRequest request) { return manager.GetUserCase(request);}

    @Override
    public CaseServiceResponse AgentActionOnCase(AgentActionRequest request) { return manager.AgentAction(request);}
}
