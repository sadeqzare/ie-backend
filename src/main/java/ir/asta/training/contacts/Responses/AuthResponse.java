package ir.asta.training.contacts.Responses;

import ir.asta.training.contacts.manager.Roles;

public class AuthResponse {
    public String Token;
    public String Message;
    public Roles role;
    public Long Id;
}
