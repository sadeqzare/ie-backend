package ir.asta.training.contacts.Responses;

import ir.asta.training.contacts.manager.Roles;

public class UserResponse{
    public Long Id;
    public String Fname;
    public String Lname;
    public String Uname;
    public String Email;
    public String NationalID;
    public Roles Role;
}
