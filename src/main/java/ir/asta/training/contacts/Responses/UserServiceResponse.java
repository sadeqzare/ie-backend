package ir.asta.training.contacts.Responses;

public class UserServiceResponse {
    public String Message;
    public Object Data;

    public UserServiceResponse(String message, Object data) {
        Message = message;
        Data = data;
    }
}
