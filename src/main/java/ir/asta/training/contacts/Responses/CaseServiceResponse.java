package ir.asta.training.contacts.Responses;

public class CaseServiceResponse {
    public String Message;
    public Object Data;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public Object getData() {
        return Data;
    }

    public void setData(Object data) {
        Data = data;
    }

    public CaseServiceResponse(String message, Object data) {
        Message = message;
        Data = data;
    }
}
