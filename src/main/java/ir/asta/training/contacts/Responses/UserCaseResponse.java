package ir.asta.training.contacts.Responses;

import ir.asta.training.contacts.manager.CaseStates;
import ir.asta.training.contacts.manager.CaseTypes;

public class UserCaseResponse {
    public long Id;
    public String Title;
    public String Body;
    public CaseStates State;
    public CaseTypes Type;
    public String AgentAction;
}
