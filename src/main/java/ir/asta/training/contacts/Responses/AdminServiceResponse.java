package ir.asta.training.contacts.Responses;

public class AdminServiceResponse {
    public String Message;
    public Object Data;

    public AdminServiceResponse(String message, Object data) {
        Message = message;
        Data = data;
    }
}
