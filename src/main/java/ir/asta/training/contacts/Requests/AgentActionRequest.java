package ir.asta.training.contacts.Requests;

import ir.asta.training.contacts.manager.CaseStates;

public class AgentActionRequest {
    public long CaseId;
    public String UserToken;
    public long UserId;
    public CaseStates State;
    public String Explanation;
}
