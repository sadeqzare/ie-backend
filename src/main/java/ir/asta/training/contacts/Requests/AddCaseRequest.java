package ir.asta.training.contacts.Requests;

import ir.asta.training.contacts.manager.CaseTypes;

public class AddCaseRequest {
    public String Title;
    public String Body;
    public int Satisfication;
    public CaseTypes Type;
    public Long UserId;
    public String UserToken;
    public Long AgentId;
}
