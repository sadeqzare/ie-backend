package ir.asta.training.contacts.Requests;

public class EditProfileRequest {
    public Long UserId;
    public String UserToken;
    public String Fname;
    public String Lname;
    public String Password;
}
