package ir.asta.training.contacts.Requests;

import ir.asta.training.contacts.manager.Roles;

public class SignupRequest {
    public String Fname;
    public String Lname;
    public String Uname;
    public String Email;
    public String NID;
    public String Password;
    public Roles Role;
}
